import { defineStore } from 'pinia'

export const useTransactionStore = defineStore('transaction', {
  state: () => ({
        transactions: [] 
      }),
  getters: {
    total() {
      return this.transactions.reduce((acc, transaction) => acc + transaction.amount, 0)
    },
    income() {
      return this.transactions
        .filter((transaction) => transaction.amount > 0)
        .reduce((acc, transaction) => acc + transaction.amount, 0)
        .toFixed(2)
    },
    expenses() {
      return this.transactions
        .filter((transaction) => transaction.amount < 0)
        .reduce((acc, transaction) => acc + transaction.amount, 0)
        .toFixed(2)
    }
  },
  
  actions: {
    loadTransactionsFromLocalStorage() {
      const savedTransactions = JSON.parse(localStorage.getItem('transactions'))
      if (savedTransactions) {
        this.transactions = savedTransactions
      }
    },

    addTransaction(transactionData) {
        console.log('addTransaction', transactionData, this.transactions)
      this.transactions.push({
        id: generateUniqueId(),
        text: transactionData.text,
        amount: transactionData.amount
      })
    },

    deleteTransaction(id) {
      this.transactions = this.transactions.filter((transaction) => transaction.id !== id)
    },

    saveTransactionsToLocalStorage() {
      localStorage.setItem('transactions', JSON.stringify(this.transactions))
    }
  }
})


function generateUniqueId() {
    return Math.floor(Math.random() * 1000000)
  }